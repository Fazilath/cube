import React, { useState } from 'react';
import { Alert, Button, Select, Spin } from 'antd';
import { useQuery } from '@apollo/react-hooks';
import { withRouter } from 'react-router-dom';
import ExploreQueryBuilder from '../components/QueryBuilder/ExploreQueryBuilder';
import { GET_DASHBOARD_ITEM } from '../graphql/queries';
import TitleModal from '../components/TitleModal.js';
const { Option } = Select;
const ExplorePage = withRouter(({ history, location }) => {
  const [addingToDashboard, setAddingToDashboard] = useState(false);
  const params = new URLSearchParams(location.search);
  const itemId = params.get('itemId');
  const { loading, error, data } = useQuery(GET_DASHBOARD_ITEM, {
    variables: {
      id: itemId,
      
    },
    skip: !itemId,
  });
  const [vizState, setVizState] = useState(null);
const [dashboardId, setDashboardId]=useState("1");
  const finalVizState =
    vizState ||
    (itemId && !loading && data && JSON.parse(data.dashboardItem.vizState)) ||
    {};
  const [titleModalVisible, setTitleModalVisible] = useState(false);
  const [title, setTitle] = useState(null);
  const finalTitle =
    title != null
      ? title
      : (itemId && !loading && data && data.dashboardItem.name) || 'New Chart';

  if (loading) {
    return <Spin />;
  }

  if (error) {
    return <Alert type="error" message={error.toString()} />;
  }

  const getKeyvalue = () =>
    JSON.parse(window.localStorage.getItem('keyvalue'))
  console.log(getKeyvalue());


  const getDashboardItems = () =>
    JSON.parse(window.localStorage.getItem('dashboardItems'));
  console.log(getDashboardItems());

  // const dashboardScreen = () => {
  //   const items = getKeyvalue();
  //   const options = [];
  //   for (var i = 0; i < key.length; i++) {

  //     options.push(<Option value="items[i].dashboardId"> items[i].name</Option>);
  //   }
  //   return options;

  // }

  //   const getDashboards = () =>
  //   JSON.parse(window.localStorage.getItem('dashboards'));
  //   console.log(getDashboards());

  //   const prepareOptions =()=>{
  // const items = getDashboardItems();
  // const options = [];
  //     for(var i=0; i<items.length;i++){

  //       options.push(<Option value="items[i].dashboardId"> items[i].name</Option>);
  //     }
  // return options;

  //   }

  //   const prepareDashboardOptions =()=>{
  //     const dashboards = getDashboards();
  //     console.log(dashboards);
  //     const dashboardOptions = [];
  //         for(var i=0; i<dashboards.length;i++){

  //           dashboardOptions.push(<Option value="items[i].dashboardId"> items[i].dashboardId</Option>);
  //         }
  //     return dashboardOptions;

  //       }
  const items = getDashboardItems();
  // console.log(prepareOptions());
  // const items = getKeyvalue();

  // const dashboards=getDashboards();
  // console.log(dashboards);


  // function key(){
  //   return(
  //     if(getKeyvalue()===Dashbard1){

  //     }
  //   )
  // }
  return (
    <div>
      <TitleModal
        history={history}
        itemId={itemId}
       dashboardId={dashboardId}
        titleModalVisible={titleModalVisible}
        setTitleModalVisible={setTitleModalVisible}
        setAddingToDashboard={setAddingToDashboard}
        finalVizState={finalVizState}
        setTitle={setTitle}
        finalTitle={finalTitle}
      />
      <ExploreQueryBuilder
        vizState={finalVizState}
        setVizState={setVizState}
        chartExtra={[
          <Select
            placeholder="Select a dashboard"
            loading={addingToDashboard}
            setAddingToDashboard={setAddingToDashboard}
            onChange={(value)=>{setDashboardId(value)
              setTitleModalVisible(true)}}
         
          >
            {/* {
              items.map(item => {
                console.log(item);
                return <Option value={item.dashboardId}> {item.dashboardId}</Option>
              })
              
            } */}
               <option value="1">
              Dashboard1
            </option>
              <option value="2">
              Dashboard2
            </option>

            <option value="3">
              Dashboard3
            </option>
          </Select>
        ]}
      />


        
       
    

      {/* <ExploreQueryBuilder
        vizState={finalVizState}
        setVizState={setVizState}
        chartExtra={[
          <Button
            key="button"
            type="primary"
            loading={addingToDashboard}
            onClick={() => setTitleModalVisible(true)}
          >
            {itemId ? 'Update' : 'Add to dashboard'}
          </Button>,
        ]}
      /> */}

    </div>
  );
});
export default ExplorePage;

