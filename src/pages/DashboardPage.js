import React, { useState, useEffect } from 'react';
import { Spin, Button, Alert } from 'antd';
import { Link } from 'react-router-dom';
import { useQuery } from '@apollo/react-hooks';
import { Icon } from '@ant-design/compatible';
import { GET_DASHBOARD_ITEMS } from '../graphql/queries';
import ChartRenderer from '../components/ChartRenderer';
import Dashboard from '../components/Dashboard';
import DashboardItem from '../components/DashboardItem';


const deserializeItem = (i, dashboardId) => {
  console.log('deserializeItem', dashboardId, i);
  return (
    i.dashboardId === dashboardId && {
      ...i,
      layout: JSON.parse(i.layout) || {},
      vizState: JSON.parse(i.vizState),
    }
  );
};

const defaultLayout = (i, dashboardId) => {
  return (
    i.dashboardId === dashboardId && {
      x: i.layout.x || 0,
      y: i.layout.y || 0,
      w: i.layout.w || 4,
      h: i.layout.h || 8,
      minW: 4,
      minH: 8,
    }
  );
};


const DashboardPage = () => {
  const { loading, error, data } = useQuery(GET_DASHBOARD_ITEMS);


  const [dashboardData, setDashboardData] = useState();
  const [dashboardId, setDashboardId] = useState('1')

  const getDashboardId = () =>
    localStorage.getItem('keyvalue') || "1"
  console.log(getDashboardId(), 'id');


  // const [dashboardId, setDashboardId] = useState(getDashboardId());
  // setDashboardId(getDashboardId());
  useEffect(() => {
    // const { loading, error, data } = useQuery(GET_DASHBOARD_ITEMS);
    setDashboardData(data);
    setDashboardId(getDashboardId());
    const myArray = dashboardData
      ? dashboardData.dashboardItems.filter((item) => item.dashboardId === dashboardId)
      : [];
    console.log('myArray', dashboardData, dashboardId, myArray);

  }, []);

  const myArray = data && data.dashboardItems.filter((item) => item.dashboardId === getDashboardId());
  console.log('myArray', myArray);

  let myData = data;
  myData && myData.dashboardItems.filter(item => {
    console.log(item, item.dashboardId === getDashboardId(), "item");
    return item.dashboardId === getDashboardId()

  })

  console.log(myData && myData.dashboardItems.filter(item => {
    return item.dashboardId === getDashboardId()

  }));

  let filteritem = myData && myData.dashboardItems.filter(item => {
    console.log(item, item.dashboardId === getDashboardId(), "item");
    return item.dashboardId === getDashboardId()

  })


  console.log(filteritem, "filteritem");
  console.log(myData, "myData");
  console.log(JSON.stringify(data), "hhhh");
  if (loading) {
    return <Spin />;
  }

  if (error) {
    return (
      <Alert
        message="Error occured while loading your query"
        description={error.toString()}
        type="error"
      />
    );
  }

  const dashboardItem = (item) => {

    console.log(item, item && getDashboardId() === item.dashboardId, "dasgboardid")
    return (

      <div key={item.id} data-grid={
        defaultLayout(item, getDashboardId())}>
        <DashboardItem key={item.id} itemId={item.id} title={item.name} dashboardId={item.dashboardId}>
          <ChartRenderer vizState={item.vizState} />
        </DashboardItem>
      </div>)

  }

  const Empty = () => (
    <div
      style={{
        textAlign: 'center',
        padding: 12,

      }}
    >
      <h2>There are no charts on this dashboard</h2>
      <Link to="/explore">
        <Button type="primary" size="large" icon={<Icon type="plus" />}>
          Add chart
        </Button>
      </Link>
    </div>
  );

  return !data || data.dashboardItems.length ? (
    <Dashboard dashboardItems={data && data.dashboardItems}>
      {data &&
        data.dashboardItems.map((i) => deserializeItem(i, getDashboardId())).map(dashboardItem)}
    </Dashboard>
  ) : (
      <Empty />
    );
};

export default DashboardPage;
