import React from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import { Layout, Menu } from 'antd';

class Header extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      current: 'mail',
    }

  };

  componentDidMount() {
    const getKeyvalue = () => {
      let value = JSON.parse(window.localStorage.getItem('keyvalue'))
      return value;
    }
    getKeyvalue()
  }
  handleClick = e => {
    // console.log('click ', e);
    this.setState({ current: e.key });
    window.localStorage.setItem('keyvalue', JSON.stringify(e.key));
  };






  render() {


    const { current } = this.state;
    return (


      <Layout.Header
        style={{
          padding: '0 32px',
        }}
      >
        <div
          style={{
            float: 'left',
          }}
        >
          <h2
            style={{
              color: '#fff',
              margin: 0,
              marginRight: '1em',
              display: 'inline',
              width: 100,
              lineHeight: '54px',
            }}
          >
            My Dashboard
        </h2>
        </div>
        <Menu
          onClick={this.handleClick}
          selectedKeys={[current]}

          theme="dark"
          mode="horizontal"
          //  selectedKeys={[location.pathname]}
          style={{
            lineHeight: '64px',
          }}
        >


          {/* <Menu.Item key='Explore'    >
        <Link to="/explore">Explore</Link>
      </Menu.Item>
      <Menu.Item key="Dashboard1">
        <Link to="/" >Dashboard1</Link>
      </Menu.Item>
      <Menu.Item key="Dashboard2">
        <Link to="/Dashboard2" >Dashboard2</Link>
      </Menu.Item> 
      <Menu.Item key="Dashboard3">
        <Link to="/Dashboard3" >Dashboard3</Link>
      </Menu.Item>  */}

          <Menu.Item key="/explore">
            <Link to="/explore">Explore</Link>
          </Menu.Item>
          <Menu.Item key="1">
            <Link to="/" onclick={() => localStorage.setItem("dashboard1", 1)}>Dashboard1</Link>
          </Menu.Item>
          <Menu.Item key="2">
            <Link to="/Dashboard2" onclick={() => localStorage.setItem("dashboard2", 2)}>Dashboard2</Link>
          </Menu.Item>
          <Menu.Item key="3">
            <Link to="/Dashboard3" onclick={() => localStorage.setItem("dashboard3", 3)}>Dashboard3</Link>
          </Menu.Item>
          <Menu.Item key="4">
            <Link to="/Dashboard4" onclick={() => localStorage.setItem("dashboard4", 4)}>Dashboard4</Link>
          </Menu.Item>


        </Menu>

      </Layout.Header>

    );
  }
}


export default withRouter(Header);
