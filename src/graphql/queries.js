import gql from 'graphql-tag';
export const GET_DASHBOARD_ITEMS = gql`
  query GetDashboardItems {
    dashboardItems {
      id
      layout
      vizState
      name
      dashboardId
      dashboardname
    }
  }
`;
export const GET_DASHBOARD_ITEM = gql`
  query GetDashboardItem($id: String!) {
    dashboardItem(id: $id) {
      id
      layout
      vizState
      name
      dashboardId
      dashboardname
    }
  }
`;
// export const CREATE_DASHBOARD = gql`
//   query CreateDashboard($id: String!) {
//     CreateDashboard(id: $id) {
//       id
//       name
//       dashboardId
//       dashboardname
//       dashboarditems
//     }
//   }
// `;